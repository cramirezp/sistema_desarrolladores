package demo_gob.models;
import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
@Table(name =" Desarrolladores")


public class Usuarios {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="nombre", length = 60, nullable = false)
    private String nombre;
    @Column(name="edad", length = 60, nullable = false)
    private String edad;
    @Column(name="habilidad", nullable = false, unique = true)
    private String habilidad;  
}
