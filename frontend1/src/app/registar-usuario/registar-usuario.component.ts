import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registar-usuario',
  templateUrl: './registar-usuario.component.html',
  styleUrls: ['./registar-usuario.component.css']
})
export class RegistrarUsuarioComponent implements OnInit{

  usuario : Usuario = new Usuario();

  constructor(private usuarioServicio: UsuarioService, private router: Router ){}

  ngOnInit(): void {
      console.log(this.usuario);
  }

  guardarUsuario(){
    this.usuarioServicio.registrarUsuario(this.usuario).subscribe(dato =>{
      console.log(dato);
      this.irALaListaDeUsuarios();
    }, error => console.log(error));
    //);
  }

  irALaListaDeUsuarios(){
    this.router.navigate(['/usuarios']);
  }

  onSubmit(){
    console.log(this.usuario);
    this.guardarUsuario();
  }

}
