import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActualizarUsuarioComponent } from './actualizar-usuario/actualizar-usuario.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { RegistrarUsuarioComponent } from './registar-usuario/registar-usuario.component';

const routes: Routes = [
  {path : 'usuarios', component:ListaUsuariosComponent},
  {path : 'actualizar-usuario/:id', component : ActualizarUsuarioComponent},
  {path : '', redirectTo: 'usuarios', pathMatch: 'full'},
  {path : 'registrar-usuario', component : RegistrarUsuarioComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
