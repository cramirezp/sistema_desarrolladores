package demo_gob.controller;

import demo_gob.models.Usuarios;
import demo_gob.repository.IUsuarioRepository;
import demo_gob.Usuarios_excepciones.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "http://localhost:4200/")
public class UsuarioController {
    @Autowired
    private IUsuarioRepository repository;
    // Este metodo sirve para listar todos los usuarios
    @GetMapping("/usuarios")
    public List<Usuarios> listarTodosLosUsuarios(){

        return repository.findAll();
    }

    // este metodo sirve para guardar el usuario
    @PostMapping("/usuarios")
    public  Usuarios guardarUsuario(@RequestBody Usuarios usuario){


        return repository.save(usuario);
    }



    // este metodo sirve para buscar a un usuario por ID
    @GetMapping("/usuarios/{id}")
    public ResponseEntity<Usuarios> obtenerUsuarioPorId(@PathVariable Long id){
        Usuarios usuario = repository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("No existe el usuario con el el ID: "+ id));
        return  ResponseEntity.ok(usuario);
    }



    // este metetodo sirve para actualizar un usuario por ID
    @PutMapping("/usuarios/{id}")
    public ResponseEntity<Usuarios> actualizarUsuario(@PathVariable Long id, @RequestBody Usuarios detallesUsuario){
        Usuarios usuario = repository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("No existe el usuario con el el ID: "+ id));
        
        
        
        
       
        usuario.setNombre(detallesUsuario.getNombre());
        usuario.setEdad(detallesUsuario.getEdad());
        usuario.setHabilidad(detallesUsuario.getHabilidad());

        Usuarios usuarioActualizado = repository.save(usuario);
        return  ResponseEntity.ok(usuarioActualizado);
    }

    //este metodo sirve para eliminar un empleado
    @DeleteMapping("/usuarios/{id}")
    public ResponseEntity<Map<String,Boolean>> eliminarUsuario(@PathVariable Long id){
        Usuarios usuario = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("No existe el usuario con el ID : " + id));

        repository.delete(usuario);
        Map<String, Boolean> respuesta = new HashMap<>();
        respuesta.put("eliminar",Boolean.TRUE);
        return ResponseEntity.ok(respuesta);
    }

}
