package demo_gob.repository;
import demo_gob.models.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface IUsuarioRepository extends JpaRepository<Usuarios, Long> {
    
}
